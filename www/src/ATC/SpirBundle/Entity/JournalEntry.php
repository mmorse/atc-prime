<?php
// src/ATC/SpirBundle/Entity/JournalEntry.php

namespace ATC\SpirBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity

 */
class JournalEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	/**
     * @ORM\ManyToOne(targetEntity="Step")
     * @var Path|null
     */
     private $Step;
/**
     * @ORM\Column(type="string", length=255)
     */
    protected $group;


	/**
     * @ORM\Column(type="string", length=255)
     */
    protected $meta;
	
	/**
     * @ORM\Column(type="string", length=255)
     */
    protected $tags;

	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $title;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $text;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $lastdate;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $datecreated;
 	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="journies")
     * @var User|null
     */
     private $user;
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
	   /**
     * @ORM\Column(name="is_private", type="boolean")
     */
    private $isPrivate;
	
	
    public function __construct()
    {
        parent::__construct();
		
         $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
   
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
