<?php
// src/ATC/SpirBundle/Entity/Step.php

namespace ATC\SpirBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 */
class Step
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the Agency name.")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The Title is too short.",
     *     maxMessage="The Title is too long."
     * )
     */
    protected $title;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $titlepos;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
	 
    protected $desc;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $descpos;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $type;

	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $datecreated;
 	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ratings")
     * @var User|null
     */
     private $user;
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
	/**
     * @ORM\Column(name="is_journalable", type="boolean")
     */
    private $isJournalable;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
	 
    protected $background;
	
    public function __construct()
    {
        parent::__construct();
		
         $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
   
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
