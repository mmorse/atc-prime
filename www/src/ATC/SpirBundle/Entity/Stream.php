<?php
// src/ATC/SpirBundle/Entity/Stream.php

namespace ATC\SpirBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 */
class Stream
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $dateactual;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $datecasual;
	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="streams")
     * @var User|null
     */
     private $subjectuser;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $verb;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $preposition;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $directobject; // can this be an array with type and object?
		/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="objectstreams")
     * @var User|null
     */
     private $douser;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $datecreated;
 
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
	
	
    public function __construct()
    {
        parent::__construct();
		
         $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
   
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
