<?php

namespace ATC\SpirBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Response;
  use  Symfony\Component\HttpFoundation\JsonResponse;

class WelcomeController extends Controller
{
    public function indexAction()
    {
        /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return $this->render('ATCSpirBundle:Welcome:index.html.twig');
    }
	
	 public function updateschemaAction()
    {
      $kernel = $this->get('kernel');
            $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
            $application->setAutoExit(false);
            //Create de Schema 
            $options = array('command' => 'doctrine:schema:update',"--force" => true);
            $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
            //Loading Fixtures
            $options = array('command' => 'doctrine:fixtures:load',"--append" => true);
            $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
        return $this->render('ATCSpirBundle:Welcome:index.html.twig');
    }
	
	 public function usersjsonAction()
    {
			$myuser=array('username'=>'mmorse','name'=>'marshal','id'=>'1'); 
			$text="This is my text"; 
			
			$data=array('user'=>$myuser, 'id'=>'1', 'text'=>$text); 
			$dataobject=  json_decode (json_encode ($data), FALSE);
			
			
			
		
		return new JsonResponse(array('data' => $data));
		
    }
	
}
