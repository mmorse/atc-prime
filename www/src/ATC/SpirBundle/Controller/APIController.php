<?php

namespace ATC\SpirBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Response;
  use  Symfony\Component\HttpFoundation\JsonResponse;

class APIController extends Controller
{
    public function indexAction()
    {
        /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return $this->render('ATCDemoBundle:Welcome:index.html.twig');
    }
	
	
	 public function userAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if($user) {
            return new JsonResponse(array(
                'id' => $user->getId(),
                'username' => $user->getUsername()
            ));
        }

        return new JsonResponse(array(
            'message' => 'User is not identified'
        ));
	}
	 public function usersjsonAction()
    {
			$myuser=array('username'=>'mmorse','name'=>'marshal','id'=>'1'); 
			$text="This is my text"; 
			
			$data=array('user'=>$myuser, 'id'=>'1', 'text'=>$text); 
			
				$dataobject=(object)$data; 
				$textdata=@'{"data":[{"user":{"username":"mmorse","name":"marshal","id":"1"},"id":"1","text":"This is my text"},{"user":{"username":"mmorse","name":"marshal","id":"1"},"id":"2","text":"This is my text2"}]}';
			
			$response = new Response($textdata);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
    }
	 public function getpromptsAction()
    {
			
			
			$pages=array(array('title'=>'Blind Bartimaeus: Preparation', 'titlepos'=>375, 'desc'=>'To prepare for this meditation find your Bible and a quiet place to read. Put your phone in "do not disturb" mode, and eliminate any possible distractions. Try to sit for a minute or two with your eyes closed, or in the dark.','bgImage'=>'bg1', 'descpos'=>325), array('title'=>'Blind Bartimaeus: Meditation', 'titlepos'=>375, 'desc'=>'The Bible tells a story of Blind Bartimaeus, a beggar who sat in the dust along side a road near Jericho. One day Jesus was passing by, and he shouted out “Jesus Christ, Son of David, have mercy on me.” As he shouted, some in the crowed told him to be quiet, but he shouted all the louder. “Son of David, have mercy on me.” Jesus stopped, and called him over. The crowd said to him, “Don’t be afraid, Jesus is calling for you.” So he threw of his cloak and went over. ”What would you have me do for you?” Jesus asked.','bgImage'=>'bg1', 'descpos'=>325),array('title'=>'Blind Bartimaeus: Reflection', 'titlepos'=>375, 'desc'=>'Imagine yourself in this story. If you have time, read it slowly twice in Mark 10:46-52. Try to imagine yourself as Blind Bartimaeus sitting on the side of the dusty dirt road. Journal and pray: what would you have Jesus do for you today? What is your greatest need? Why?','bgImage'=>'bg1', 'descpos'=>325), array('title'=>'Blind Bartimaeus: Invitation', 'titlepos'=>375, 'desc'=>'Periodically between now and your next time of prayer, close your eyes and imagine what it might be like not to see. Realize that we need to have our spiritual eyes open to Christ. He meets us in our place of need. In addition, he calls us to recognize the need of those around us.','bgImage'=>'bg1', 'descpos'=>325)); 
			$text="Short Description Text"; 
			$prompt=array('id'=>'1', 'title'=>'Blind Bartamaeus - What do you want?', 'pages'=>$pages, 'text'=>$text); 
			
			
			
				$textdata=@'{"data":['. json_encode ($prompt) . ']}';
			
			$response = new Response($textdata);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
    }
	
	 public function getstreamsAction()
    {
	
			$myuser=array('username'=>'mmorse','name'=>'marshal','id'=>'1'); 
			
			$streams=array(array('id'=>'1','dateactual'=>date("Y-m-d H:i:s"), 'datecasual'=>'Today',  'subjectcannonical'=>'Marshal Morse', 'subjectcasual'=>'Marshal', 'verb'=>'completed', 'preposition'=>'', 'directobject'=>'Blind Bartamaeus','user'=>$myuser 
			
			
			)); 
			$text="Short Description Text"; 
			
			
			
				$textdata=@'{"data":'. json_encode ($streams) . '}';
			
			$response = new Response($textdata);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
    }
	
	 public function getpathsAction()
    {
		
	
	
			$myuser=array('username'=>'mmorse','name'=>'marshal','id'=>'1'); 
			$myuserpathmeta=array('active'=>'YES','lastentry'=>date("Y-m-d H:i:s"),'lastprompt'=>0, 'promptscomplete'=>0);
			
			$paths=array(array('id'=>'1','datecreated'=>date("Y-m-d "), 'title'=>'Daily Walk 2014',  'subtitle'=>date("Y-m-d H:i:s"), 'description'=>'A new prompt for each day of the year', 'type'=>'calendar', 'level'=>'free', 'userpathmeta'=>$myuserpathmeta,'user'=>$myuser, 'totalprompts'=>365
			
			
			)); 
			$text="Short Description Text"; 
			
			
			
				$textdata=@'{"data":'. json_encode ($paths) . '}';
			
			$response = new Response($textdata);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
    }
 public function createpathAction()
    {
		
	
	
			$myuser=array('username'=>'mmorse','name'=>'marshal','id'=>'1'); 
			$myuserpathmeta=array('active'=>'YES','lastentry'=>date("Y-m-d H:i:s"),'lastprompt'=>0, 'promptscomplete'=>0);
			
			$paths=array(array('id'=>'1','datecreated'=>date("Y-m-d "), 'title'=>'ThisisaNewPath',  'subtitle'=>date("Y-m-d H:i:s"), 'description'=>'A new prompt for each day of the year', 'type'=>'calendar', 'level'=>'free', 'userpathmeta'=>$myuserpathmeta,'user'=>$myuser, 'totalprompts'=>365
			
			
			)); 
			$text="Short Description Text"; 
			
			
			
				$textdata=@'{"data":'. json_encode ($paths) . '}';
			
			$response = new Response($textdata);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
    }
	
}
